<?php

/**
 * @file
 * Administrative page callbacks for the BarTable module.
 */

function bartable_admin() {
  $form = array();

  $form['bartable_classes'] = array(
    '#title' => t('Classes'),
    '#type' => 'textarea',
    '#description' => t('Enter the CSS table classes, one per line, which should be converted when viewed on a mobile device.'),
    '#default_value' => variable_get('bartable_classes', ''),
  );

  $form['bartable_ids'] = array(
    '#title' => t('IDs'),
    '#type' => 'textarea',
    '#description' => t('Enter the CSS table IDs, one per line, which should be converted when viewed on a mobile device.'),
    '#default_value' => variable_get('bartable_ids', ''),
  );

  return system_settings_form($form);
}
